# DataHub

DataHub is a pilot/dummy web app for data teams involving analysts, engineers to execute queries, learn from data, and export results.

The project is built with React and Typescript.

Deployed application URL: https://datahub-one.vercel.app/ OR https://datahub-one-sigma.vercel.app/

## Features

- Query Editor
- Data results in a tabular form
- History
- Saved Queries
- View schema
- Export to CSV

## Demo 

https://www.loom.com/share/d5fc3f60c7494668bc510a526d620ad3?sid=73802887-4a17-49b9-9a12-7b5c24ae4d27

If the above link does not work, [click here](assets/demo.mp4)

## Libraries

- React: A JS UI library for building SPAs
- Zustand: State management for React. Easy to use
- Tailwind: Utility classes for basic styling
- Tanstack Table: Table component for rendering large amount of rows
- Tanstack Virtual: Enables virtualisation on large lists of data
- Faker: Generate mock data using utility functions
- Export to CSV: To generate a CSV and download it

## Screenshots

![Home page](assets/image1.png)
![Schema modal](assets/image2.png)

## Performance

The following metrics were measured on the Performance tab on DevTools:

- DOMContentLoaded: 149ms
- Load event: 149ms
- LCP: 192ms
- FP: 192ms
- FCP: 192ms

![Report](assets/report-image.png)

Lighthouse report: [PDF](assets/lh-report.pdf)
_Note: Metrics may change depending on the CPU and Network conditions._

Reasons for having fast load times are due to the following:

1. Lazy loading: Components/library imports can be lazy loaded i.e the browser only fetches them when they are required. This saves us processing time on the client and initial load time decreases. One such library `export-to-csv` in this project has been lazy loaded only when the `Export` button is clicked.

2. Code splitting: The application can split into different chunks and be loaded whenever required. This also helps in making the initial load fast. This is not done as of now due to the requirement and nature of the project but must be considered when building it for production.

3. Compiled CSS on build: Utility classes from Tailwind are generated on build time and removes unused CSS allowing for faster page loads.
