import { DataType, generateAirlineData, generatePeopleData } from "./mock";

export const getDataFromQuery = (type: DataType) => {
  return type === DataType.PERSON
    ? generatePeopleData(Math.floor(Math.random() * 4000))
    : generateAirlineData(Math.floor(Math.random() * 4000));
};
