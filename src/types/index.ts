import { Airline, DataType, Person } from "../utils/mock";

export type QueryItemProps = {
  text: string;
  onClick: React.MouseEventHandler<HTMLSpanElement>;
};

export type Store = {
  query: string;
  results: (Person | Airline)[];
  history: string[];
  runQuery: (query: string) => void;
  savedQueries: string[];
  saveQuery: (query: string) => void;
  schema: (type: DataType) => Record<string, string>;
};
