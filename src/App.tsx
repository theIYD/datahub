import Header from "./components/Header";
import Playground from "./components/Playground";
import Sidebar from "./components/Sidebar";

function App() {
  return (
    <main className="flex flex-col h-screen mx-4 my-4">
      <Header />
      <div className="flex h-full">
        <Playground />
        <Sidebar />
      </div>
    </main>
  );
}

export default App;
