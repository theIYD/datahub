import { create } from "zustand";
import { getDataFromQuery } from "../../utils/query";
import { Store } from "../../types";
import { DataType, DefaultSavedQueries } from "../../utils/mock";

export const useDataStore = create<Store>((set) => ({
  query: "",
  savedQueries: DefaultSavedQueries,
  results: [],
  history: [],
  runQuery: (query) => {
    const queryDataType = query.includes("people")
      ? DataType.PERSON
      : DataType.AIRLINE;
    const results = getDataFromQuery(queryDataType);

    set((state) => ({
      ...state,
      results,
      history: [query, ...state.history],
      query,
    }));
  },
  saveQuery: (query: string) => {
    set((state) => ({
      ...state,
      savedQueries: [query, ...state.savedQueries],
    }));
  },
  // @ts-expect-error - fix type
  schema: (type: DataType) => {
    return type === DataType.PERSON
      ? {
          id: "int",
          firstName: "varchar",
          lastName: "varchar",
          age: "int",
          visits: "int",
          progress: "int",
          status: "listOf(relationship, complicated, single)",
          createdAt: "Date",
        }
      : {
          id: "int",
          airline: "varchar",
          iataCode: "varchar",
          airplane: "varchar",
          flightNumber: "varchar",
        };
  },
}));
