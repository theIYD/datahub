export default function Button(
  props: React.ComponentPropsWithoutRef<"button">
) {
  return (
    <button
      {...props}
      className={`inline-flex items-center rounded-md bg-slate-800 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-slate-600 focus-visible:outline focus-visible:outline-2 transition duration-500 ease-in-out focus-visible:outline-offset-2 focus-visible:outline-slate-800 ${props.className}`}
    />
  );
}
