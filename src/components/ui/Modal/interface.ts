export interface ModalProps {
  visible: boolean;
  title: string;
  content: JSX.Element;
  onClose: () => void;
}
