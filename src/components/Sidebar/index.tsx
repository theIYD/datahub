import QueryList from "../QueryList";

export default function Sidebar() {
  return (
    <div className="w-1/4">
      <QueryList />
    </div>
  );
}
