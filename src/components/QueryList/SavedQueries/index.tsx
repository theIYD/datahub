import { useDataStore } from "../../../store/query";
import QueryItem from "../QueryItem";

export default function SavedQueries() {
  const { savedQueries, runQuery } = useDataStore((state) => ({
    savedQueries: state.savedQueries,
    runQuery: state.runQuery,
  }));

  const handleQueryClick = (query: string) => {
    runQuery(query);
  };

  return (
    <div className="flex flex-col">
      <span className="text-lg font-medium">Saved</span>
      <div className="flex flex-col mt-4 overflow-y-auto max-h-[300px]">
        {savedQueries.map((query, index) => {
          return (
            <QueryItem
              key={`${query}-${index}`}
              text={query}
              onClick={() => handleQueryClick(query)}
            />
          );
        })}
      </div>
    </div>
  );
}
