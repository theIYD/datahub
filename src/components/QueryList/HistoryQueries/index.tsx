import { useDataStore } from "../../../store/query";
import QueryItem from "../QueryItem";

export default function HistoryQueries() {
  const { historyQueries, runQuery } = useDataStore((state) => ({
    historyQueries: state.history,
    runQuery: state.runQuery,
  }));

  const handleQueryClick = (query: string) => {
    runQuery(query);
  };

  return (
    <div className="flex flex-col h-full">
      <span className="text-lg font-medium">History</span>
      <div className="flex flex-col mt-4 overflow-y-auto max-h-[300px]">
        {historyQueries.length === 0
          ? null
          : historyQueries.map((query, index) => {
              return (
                <QueryItem
                  key={`${query}-${index}`}
                  text={query}
                  onClick={() => handleQueryClick(query)}
                />
              );
            })}
      </div>
    </div>
  );
}
