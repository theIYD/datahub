import HistoryQueries from "./HistoryQueries";
import SavedQueries from "./SavedQueries";

export default function QueryList() {
  return (
    <div className="flex flex-col bg-slate-100 rounded-md h-full flex-shrink-0 max-h-screen p-4">
      <SavedQueries />
      <hr className="my-4" />
      <HistoryQueries />
    </div>
  );
}
