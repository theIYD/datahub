import { QueryItemProps } from "../../../types";

export default function QueryItem({ text, onClick }: QueryItemProps) {
  return (
    <span
      onClick={onClick}
      className="py-4 px-6 bg-slate-800 mb-4 rounded-md cursor-pointer text-white transition duration-500 ease-in-out hover:bg-slate-500"
    >
      {text}
    </span>
  );
}
