export default function Header() {
  return (
    <div className="w-full rounded-md mb-4 p-4 bg-slate-100 flex items-center justify-between">
      <span className="font-extrabold text-2xl">DataHub</span>
      <div className="flex space-x-6 mr-6">
        <a href="#">Home</a>
        <a href="#">Databases</a>
      </div>
    </div>
  );
}
