import Editor from "./Editor";
import QueryResult from "./QueryResult";

export default function Playground() {
  return (
    <div className="w-3/4 h-full bg-slate-100 mr-4 rounded-md flex flex-col">
      <Editor />
      <QueryResult />
    </div>
  );
}
