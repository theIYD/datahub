import { useDataStore } from "../../../store/query";
import Button from "../../ui/Button";

export default function Editor() {
  const runQuery = useDataStore((state) => state.runQuery);

  const handleQuerySubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    const queryValue = event.currentTarget.query.value;
    runQuery(queryValue);
  };

  return (
    <form onSubmit={handleQuerySubmit} className="w-full p-4">
      <div className="flex space-x-4 w-full">
        <div className="flex-1 rounded-lg shadow-sm ring-gray-300">
          <label htmlFor="query" className="sr-only">
            Query
          </label>
          <textarea
            required
            name="query"
            rows={5}
            id="query"
            className="block w-full p-2 resize-none border border-solid border-slate-400 rounded-md bg-transparent py-1.5 text-gray-900 placeholder:text-gray-400"
            placeholder="Enter your query here..."
          />
        </div>
        <div className="flex flex-col">
          <Button type="submit" className="mb-2">
            Run
          </Button>
        </div>
      </div>
    </form>
  );
}
