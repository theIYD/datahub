import { useMemo, useRef, useState } from "react";
import { useDataStore } from "../../../store/query";
import {
  Row,
  flexRender,
  getCoreRowModel,
  getSortedRowModel,
  useReactTable,
} from "@tanstack/react-table";
import { useVirtualizer } from "@tanstack/react-virtual";
import Button from "../../ui/Button";
import Modal from "../../ui/Modal";
import Schema from "./Schema";
import useColumns from "../../../hooks";

export default function QueryResult() {
  const { data, query, saveQuery } = useDataStore((state) => ({
    data: state.results,
    query: state.query,
    saveQuery: state.saveQuery,
  }));

  const [viewSchema, setViewSchema] = useState<boolean>(false);
  const { personTableColumns, airlineTableColumns } = useColumns();
  const tableContainerRef = useRef(null);

  const columns = useMemo(
    () => (query.includes("people") ? personTableColumns : airlineTableColumns),
    [query, data]
  );

  const table = useReactTable({
    data,
    // @ts-expect-error: fix type here
    columns,
    getCoreRowModel: getCoreRowModel(),
    getSortedRowModel: getSortedRowModel(),
    debugTable: true,
  });

  const { rows } = table.getRowModel();
  const rowVirtualizer = useVirtualizer({
    count: rows.length,
    estimateSize: () => 33, //estimate row height for scrollbar dragging
    getScrollElement: () => tableContainerRef.current,
    measureElement: (element) => element?.getBoundingClientRect().height,
    overscan: 5,
  });

  const virtualItems = rowVirtualizer.getVirtualItems();
  const totalSize = rowVirtualizer.getTotalSize();

  const handleAddToSavedClick = (query: string) => {
    saveQuery(query);
  };

  const handleOnExportClicked = async () => {
    const { mkConfig, generateCsv, download } = await import("export-to-csv");

    const csvConfig = mkConfig({ useKeysAsHeaders: true });
    const csv = generateCsv(csvConfig)(data);

    download(csvConfig)(csv);
  };

  return (
    <div className="flex flex-col px-4 pt-4">
      <span className="font-bold text-lg">Results</span>
      {data.length === 0 ? null : (
        <>
          <div className="my-4 mx-auto flex flex-col w-full h-full">
            <span>
              Showing <span className="font-bold">{data.length}</span> results.
            </span>
            <div
              className="border border-solid border-gray-500 my-4 overflow-auto relative max-h-[400px]"
              ref={tableContainerRef}
            >
              <table className="grid border-collapse border-spacing-0 table-fixed">
                <thead className="grid sticky top-0 z-10 bg-gray-500">
                  {table.getHeaderGroups().map((headerGroup) => (
                    <tr
                      key={headerGroup.id}
                      className="flex border-b border-solid border-b-gray-500"
                    >
                      {headerGroup.headers.map((header) => {
                        return (
                          <th
                            key={header.id}
                            style={{
                              width: header.getSize(),
                            }}
                            className="flex border-b border-r border-solid border-b-gray-500 border-r-gray-500 px-1 py-2 text-left"
                          >
                            <div
                              {...{
                                className: header.column.getCanSort()
                                  ? "cursor-pointer select-none"
                                  : "",
                                onClick:
                                  header.column.getToggleSortingHandler(),
                              }}
                            >
                              {flexRender(
                                header.column.columnDef.header,
                                header.getContext()
                              )}
                              {{
                                asc: " ⬆️",
                                desc: " ⬇️",
                              }[header.column.getIsSorted() as string] ?? null}
                            </div>
                          </th>
                        );
                      })}
                    </tr>
                  ))}
                </thead>
                <tbody
                  style={{
                    height: `${totalSize}px`,
                  }}
                  className="grid relative"
                >
                  {virtualItems.map((virtualRow) => {
                    const row = rows[virtualRow.index] as Row<unknown>;
                    return (
                      <tr
                        data-index={virtualRow.index}
                        ref={(node) => rowVirtualizer.measureElement(node)}
                        key={row.id}
                        style={{
                          transform: `translateY(${virtualRow.start}px)`,
                        }}
                        className="flex absolute w-full border-b border-solid border-b-gray-500"
                      >
                        {row.getVisibleCells().map((cell) => {
                          return (
                            <td
                              key={cell.id}
                              style={{
                                width: cell.column.getSize(),
                              }}
                              className="p-2 flex"
                            >
                              {flexRender(
                                cell.column.columnDef.cell,
                                cell.getContext()
                              )}
                            </td>
                          );
                        })}
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
            <div className="flex items-center justify-between">
              <div className="flex mb-2 space-x-2">
                <Button onClick={() => handleAddToSavedClick(query)}>
                  Add to Saved
                </Button>
                <Button onClick={() => setViewSchema(true)}>View Schema</Button>
              </div>
              <Button onClick={handleOnExportClicked} className="mb-2">
                Export
              </Button>
            </div>
          </div>
          <Modal
            visible={viewSchema}
            onClose={() => setViewSchema(false)}
            title="Schema"
            content={<Schema />}
          />
        </>
      )}
    </div>
  );
}
