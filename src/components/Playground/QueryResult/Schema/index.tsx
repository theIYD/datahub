import { useDataStore } from "../../../../store/query";
import { DataType } from "../../../../utils/mock";

export default function Schema() {
  const schema = useDataStore((state) => {
    return state.query.includes("people")
      ? state.schema(DataType.PERSON)
      : state.schema(DataType.AIRLINE);
  });

  return (
    <table className="grid border-collapse border-spacing-0 border border-solid border-gray-500 table-fixed">
      <thead className="grid sticky top-0 z-10 bg-gray-500 pl-6">
        <tr className="flex">
          <th className="flex px-1 py-2 w-full">name</th>
          <th className="flex px-1 py-2 w-full">type</th>
        </tr>
      </thead>
      <tbody className="grid relative">
        {Object.keys(schema).map((key) => {
          const type = schema[key];
          return (
            <tr className="flex flex-1 w-full border-b border-solid border-b-gray-500 pl-6">
              <td className="p-2 w-full">{key}</td>
              <td className="p-2 w-full italic">{type}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}
