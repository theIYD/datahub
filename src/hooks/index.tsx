import { ColumnDef } from "@tanstack/react-table";
import { Airline, Person } from "../utils/mock";

export default function useColumns(): {
  personTableColumns: ColumnDef<Person>[];
  airlineTableColumns: ColumnDef<Airline>[];
} {
  return {
    personTableColumns: [
      {
        accessorKey: "id",
        header: "ID",
        size: 60,
      },
      {
        accessorFn: (row) => row.firstName,
        accessorKey: "firstName",
        cell: (info) => info.getValue(),
        header: () => <span>First Name</span>,
      },
      {
        accessorFn: (row) => row.lastName,
        id: "lastName",
        cell: (info) => info.getValue(),
        header: () => <span>Last Name</span>,
      },
      {
        accessorKey: "age",
        header: () => <span>Age</span>,
        size: 50,
      },
      {
        accessorKey: "visits",
        header: () => <span>Visits</span>,
        size: 100,
      },
      {
        accessorKey: "status",
        header: () => <span>Status</span>,
      },
      {
        accessorKey: "progress",
        header: () => <span>Progress</span>,
        size: 80,
      },
      {
        accessorKey: "createdAt",
        header: () => <span>Created At</span>,
        cell: (info) => info.getValue<Date>().toLocaleString(),
        size: 250,
      },
    ],
    airlineTableColumns: [
      {
        accessorKey: "id",
        header: "ID",
        size: 60,
      },
      {
        accessorKey: "airline",
        cell: (info) => info.getValue(),
        header: () => <span>Airline</span>,
      },
      {
        accessorKey: "iataCode",
        cell: (info) => info.getValue(),
        header: () => <span>IATA Code</span>,
      },
      {
        accessorKey: "airplane",
        cell: (info) => info.getValue(),
        header: () => <span>Airplane</span>,
      },
      {
        accessorKey: "flightNumber",
        cell: (info) => info.getValue(),
        header: () => <span>Flight No</span>,
      },
    ],
  };
}
